package ee.kriso;

import java.lang.Math.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // ylesanne 1

        double myPi = Math.PI;

        System.out.println("Ülesande 1 vastus on:");
        System.out.println(2 * myPi);
        System.out.println();

        // ylesanne 2

        System.out.println("Ülesande 2 vastus on:");
        System.out.println(equalityNr(1, 2));
        System.out.println();

        // ylesanne 3
        // sisestamine meetodi juures allpool

        System.out.println("Ülesande 3 vastus on:");
        measurer();
        System.out.println();

        // ylesanne 4
        System.out.println("Ülesande 4 vastus on:");
        System.out.println(century(0));
        System.out.println(century(1));
        System.out.println(century(128));
        System.out.println(century(598));
        System.out.println(century(1624));
        System.out.println(century(1827));
        System.out.println(century(1996));
        System.out.println(century(2017));
        System.out.println();

        // ylesanne 5
        new Country("Eesti",15000);
//        System.out.printf("Riigi nimi on %s", countryName);

    }

    // meetod yl 2
    public static boolean equalityNr(int nr1, int nr2) {

        if (nr1 == nr2) {
            return true;
        } else {
            return false;
        }
    }

    // meetod yl 3
    public static void measurer() {
        List<String> stringList = new ArrayList<>();
        List<Integer> lengths = new ArrayList<Integer>();

        stringList.add("yks");
        stringList.add("kaks");
        stringList.add("kolm");

        for (int i = 0; i < stringList.size(); i++) {
            int nr = stringList.get(i).length();
            lengths.add(nr);
        }
        for (int i = 0; i < lengths.size(); i++) {
            System.out.println(lengths.get(i));
        }

    }

    // meetod yl 4
    public static byte century(int year) {
        if (year >= 1 && year <= 2018) {
            String yearString = Integer.toString(year);
            if (yearString.length() <= 2) return 1;
            else if (yearString.length() <= 4) return (byte) (Math.ceil((year + 99) / 100));
//            if (yearString.length()=3 return 1;
            else return -1;
        } else {
            return -1;
        }
    }
}